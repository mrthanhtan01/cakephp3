<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Core\Configure;

use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

class UserController extends AppController
{

    public function index()
    {

        $this->loadComponent('Paginator');

        $users = $this->Paginator->paginate(
            $this->User
                ->find()
                ->where(['disable =' => 0])
                ->contain(['User_address'])
        );
        $this->set(compact('users'));
    }

    public function delete() {
        if (isset($_GET['id'])) {
            // Prior to 3.6.0
            $userTable = TableRegistry::get('User');

            $userTable = TableRegistry::getTableLocator()->get('User');
            $user = $userTable->get($_GET['id']); // Return article with id 12

            $user->disable = 1;
            $userTable->save($user);
//            Configure::write('Session', [
//                'message' => 'Xóa thành công User ' .$_GET['name']
//            ]);
            $this->redirect('/user');
        } else {
            echo 'Empty User delete';
        }
    }

}
