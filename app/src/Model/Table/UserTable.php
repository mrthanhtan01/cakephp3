<?php
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\Table;
class userTable extends Table
{
    public function initialize(array $config)
    {
        $this->hasOne('User_address')
             ->setName('user_address')
             ->setDependent(true);

        $this->addBehavior('Timestamp');
    }
}
